import React from 'react';


class Row extends React.Component {
  onBtnClickHendle = ev => {
    let id = ev.currentTarget.parentElement.parentElement.id;
    this.props.cart ? this.props.onReduceItem(id - 1) : this.props.onAddItem(id - 1);
  }
  clearItem = ev => {
    let id = ev.currentTarget.parentElement.parentElement.id;
    this.props.onClearItem(id);
  };

  render() {
    const { id, name, price, numInCart } = this.props.data;

    return (
      <tr id={id} className='table__row'>
        <td>{name}</td>
        {this.props.cart ? <td>{price * numInCart}</td> : <td>{price}</td>}
        <td>{numInCart}</td>
        {
          this.props.cart ?
            <td><button onClick={this.onBtnClickHendle} className='table__addBtn btn'>&#8722;</button></td> :
            <td><button onClick={this.onBtnClickHendle} className='table__addBtn btn'>&#43;</button></td>
        }
        {
          this.props.cart ?
            <td><button onClick={this.clearItem} className='table__delAllBtn btn'>Удалить все</button></td> :
            null
        }
      </tr>
    );
  };
};

export default Row
