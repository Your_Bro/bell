import React from 'react';
import goods from './Goods';
import Row from './Row';


class ItemsList extends React.Component {
  state = {
    items: goods,
  };

  handleAddItem = id => {
    let myData = this.state.items
    ++myData[id].numInCart;
    this.setState({ items: myData })
  };

  itemsInCartCount = () => {
    let count = 0;
    this.state.items.forEach(el => {
      return el.numInCart > 0 ? count += el.numInCart : null;
    })
    return count;
  };

  goToCart = () => {
    this.props.clickGoToAnotherPageBtn()
  };

  renderItems = () => {
    let template = null;
    const data = this.state.items;

    if (data.length) {
      template = data.map(elem => {
        return <Row key={elem.id} data={elem} onAddItem={this.handleAddItem} />
      });
    } else {
      template = <tr><td colSpan='3'>Товара в наличии нет</td></tr>
    };
    return template;
  };

  render() {
    return (
      <div className="container">
        <table className="table">
          <tbody>
            <tr className="table__row">
              <th className='table__title'>Название</th>
              <th className='table__title'>Цена</th>
              <th className='table__title'>Количество</th>
            </tr>
            {this.renderItems()}
            {this.itemsInCartCount() > 0 ?
              <tr><td colSpan='3' className="itemsInCartCount"><strong>Товаров в вашей корзине: {this.itemsInCartCount()}</strong></td></tr> :
              <tr><td colSpan='3' className="itemsInCartCount"><strong>Ваша корзина пуста</strong></td></tr>
            }
          </tbody>
        </table>
        <button className="goToCart-btn btn" onClick={this.goToCart}>
          Корзина
        </button>
      </div>
    )
  }
};

export default ItemsList