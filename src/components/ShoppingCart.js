import React from 'react';
import goods from './Goods';
import Row from './Row';
class ShoppingCart extends React.Component {
  state = {
    items: goods,
  };

  handleReduceItem = id => {
    let myData = this.state.items;
    --myData[id].numInCart;
    this.setState({ items: myData });
  };

  totalCost = () => {
    let count = 0;
    this.state.items.forEach(el => {
      return el.numInCart > 0 ? count += el.numInCart * el.price : null;
    })
    return count;
  };

  goToItemList = () => {
    this.props.clickGoToAnotherPageBtn();
  };

  clearItem = id => {
    let myData = this.state.items;
    myData[id - 1].numInCart = 0;
    this.setState({ items: myData });
  };

  clearCart = () => {
    let myData = this.state.items;
    myData.forEach(el => el.numInCart = 0);
    this.setState({ items: myData });
  };

  renderItems = () => {
    let template = null;
    const data = this.state.items;

    if (data.length) {
      template = data.map(elem => {
        if (elem.numInCart > 0) {
          return <Row key={elem.id}
            cart={true}
            data={elem}
            onReduceItem={this.handleReduceItem}
            onClearItem={this.clearItem} />
        }
      });
    } else {
      template = <tr><td colSpan='3'>Товара в наличии нет</td></tr>
    };
    return template;
  };

  render() {
    return (
      <div className="container">
        <h3>Корзина</h3>
        <table className="table">
          <tbody>
            <tr className="table__row">
              <th className='table__title'>Название</th>
              <th className='table__title'>Стоимость</th>
              <th className='table__title'>Количество</th>
            </tr>
            {this.renderItems()}
            <tr><td colSpan='3' className="itemsInCartCount"><strong>Всего: {this.totalCost()}</strong></td></tr>
          </tbody>
        </table>
        <button className="goToItemList-btn btn" onClick={this.goToItemList}>
          Вернуться к списку товаров
        </button>
        <button className="clearCart-btn btn" onClick={this.clearCart}>
          Очистить корзину
        </button>
      </div>
    )
  }
};

export default ShoppingCart