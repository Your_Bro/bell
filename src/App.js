import React from 'react';
import ItemsList from './components/ItemsList';
import ShoppingCart from './components/ShoppingCart';


class App extends React.Component {
  state = {
    itemListVisible: true,
  };

  goToAnotherPageBtn = () => {
    return this.state.itemListVisible ? this.setState({ itemListVisible: false }) :
      this.setState({ itemListVisible: true })
  }

  render() {
    return this.state.itemListVisible ? < ItemsList clickGoToAnotherPageBtn={this.goToAnotherPageBtn} /> : <ShoppingCart clickGoToAnotherPageBtn={this.goToAnotherPageBtn} />;
  }
};

export default App;
